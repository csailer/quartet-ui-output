![pipeline](https://gitlab.com/serial-lab/quartet-ui-output/badges/master/pipeline.svg) | ![coverage](https://gitlab.com/serial-lab/quartet-ui-output/badges/master/coverage.svg)

# QU4RTET Output Desktop Plugin

This plugin provides the UI elements necessary to interact with the QU4RTET backend Output module.