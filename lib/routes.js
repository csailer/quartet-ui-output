"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _AuthenticationForm = require("./components/Forms/AuthenticationForm");

var _AuthenticationList = require("./components/Lists/AuthenticationList");

var _EndpointsList = require("./components/Lists/EndpointsList");

var _EPCISCriteriaList = require("./components/Lists/EPCISCriteriaList");

var _EndpointForm = require("./components/Forms/EndpointForm");

var _EPCISCriteriaForm = require("./components/Forms/EPCISCriteriaForm");

// Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
const React = qu4rtet.require("react");
const { Route } = qu4rtet.require("react-router");

exports.default = (() => {
  return [React.createElement(Route, {
    key: "addAuthentication",
    path: "/output/:serverID/add-authentication",
    component: _AuthenticationForm.AddAuthentication
  }), React.createElement(Route, {
    key: "addEndpoint",
    path: "/output/:serverID/add-endpoint",
    component: _EndpointForm.AddEndpoint
  }), React.createElement(Route, {
    key: "addEPCISCriteria",
    path: "/output/:serverID/add-criteria",
    component: _EPCISCriteriaForm.AddEPCISCriteria
  }), React.createElement(Route, {
    key: "authentication",
    path: "/output/:serverID/authentication",
    component: _AuthenticationList.AuthenticationList
  }), React.createElement(Route, {
    key: "endpointsList",
    path: "/output/:serverID/endpoints",
    component: _EndpointsList.EndpointsList
  }), React.createElement(Route, {
    key: "EPCISOutputCriteria",
    path: "/output/:serverID/epcis-output-criteria",
    component: _EPCISCriteriaList.EPCISCriteriaList
  })];
})();